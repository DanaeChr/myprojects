package com.example.springdata.springdatademo.service;

import com.example.springdata.springdatademo.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import com.example.springdata.springdatademo.repository.BookRepository;

@Service
public class DBInit implements ApplicationListener< ApplicationReadyEvent > {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void onApplicationEvent ( ApplicationReadyEvent applicationReadyEvent ) {

        Book myBook = new Book();
        myBook.setNumberOfPages( 420 );
        myBook.setAuthor( "Danae Chronopoulou" );
        myBook.setTitle( "MY GREAT BOOK" );

        bookRepository.save( myBook );
    }
}
