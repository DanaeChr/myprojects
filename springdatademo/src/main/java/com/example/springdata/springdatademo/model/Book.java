package com.example.springdata.springdatademo.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity //to make Book entity bean
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private long id;
    @Column(nullable = false)
    private int numberOfPages;
    private String author;
    @Column(unique = true, nullable = false)
    private String title;
}
