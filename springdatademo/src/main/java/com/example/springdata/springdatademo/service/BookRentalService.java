package com.example.springdata.springdatademo.service;

import com.example.springdata.springdatademo.model.Book;
import com.example.springdata.springdatademo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookRentalService implements BookService{

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List < Book > getAllBooks () {
        return bookRepository.findAll();
    }

    @Override
    public String getBookAuthorByBookTitle ( String title ) {
        return bookRepository.findBookByTitle( title ).getAuthor();

    }


}
