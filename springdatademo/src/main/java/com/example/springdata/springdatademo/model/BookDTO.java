package com.example.springdata.springdatademo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BookDTO {

    //all attributes from json
}
