package com.example.springdata.springdatademo.repository;


import com.example.springdata.springdatademo.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository < Book, Long> {

    Book findBookByTitle(String title);

}
