package com.example.springdata.springdatademo.service;

import com.example.springdata.springdatademo.model.Book;

import java.util.List;

public interface BookService {
    /**
     * Gets all the book
     * @return the list of books
     */
    List< Book > getAllBooks();

    /**
     * Gets the book by the title
     * @param title
     * @return a string
     */
    String getBookAuthorByBookTitle(String title);
}
