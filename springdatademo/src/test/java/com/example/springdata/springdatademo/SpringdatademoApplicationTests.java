package com.example.springdata.springdatademo;

import com.example.springdata.springdatademo.model.Book;
import com.example.springdata.springdatademo.repository.BookRepository;
import com.example.springdata.springdatademo.service.BookRentalService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
//@DataJpaTest
@Slf4j
public class SpringdatademoApplicationTests {

	@Autowired
	private BookRepository bookRepository;
	//private BookRentalService bookRentalService;


	@Test
	public void contextLoads() {
	}

	@Test
	public void testingFindByTitle(){

		Book myBook = new Book();
		myBook.setNumberOfPages( 420 );
		myBook.setAuthor( "Danae Chronopoulou" );
		myBook.setTitle( "MY GREAT BOOK" );

		//TESTING
//		bookRepository.save( myBook );
//		log.info( "TESTING FIND BY TITLE FOR BOOK" );
//		Book book = bookRepository.findBookByTitle( "MY GREAT BOOK");
//		log.info( "BOOK FOUND: {}", book.toString() );

		//log.info( "THE AUTHOR IS: " );


	}

}
