package gr.mycompany.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication (scanBasePackages = ("gr.mycompany.demo.controller , gr.mycompany.demo.service"))
public class DemoApplication {

    public static void main ( String[] args ) {

        SpringApplication.run( DemoApplication.class, args );
    }

}
