package gr.mycompany.demo.service;

import gr.mycompany.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Interface uses method findAllEmployees from controller
 */
public interface EmployeeService extends JpaRepository {

   List< Employee > findAllEmployees();

   Employee findEmployeeById(int id);

   void deleteEmployeeById(int id);

}
