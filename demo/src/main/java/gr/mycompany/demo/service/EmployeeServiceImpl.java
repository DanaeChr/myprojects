package gr.mycompany.demo.service;

import lombok.extern.slf4j.Slf4j;
import gr.mycompany.demo.model.Employee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("EmployeeService")
@Slf4j


public abstract class EmployeeServiceImpl implements EmployeeService{
    private static List<Employee>  employees;

    static{
        employees = populateDummyUsers();
    }


    public List<Employee> findAllEmployees(){

        return employees;
    }

    public Employee findEmployeeById(int id){

        return employees.get( id );
    }

    public void deleteEmployeeById(int id){

        employees.remove( id );
    }


    /**
     *Add employees into a list
     * @return the list of emploeyees
     */
    private static List< Employee > populateDummyUsers(){
        List<Employee> employees = new ArrayList <>(  );
        employees.add( new Employee(1, "Danae", "Dimokratias"));
        employees.add( new Employee(2, "George", "Kifisias"));
        employees.add( new Employee(3, "John", "Tatoiou"));
        employees.add( new Employee(4, "Maria", "Marathonos"));
        return employees;
    }

}
