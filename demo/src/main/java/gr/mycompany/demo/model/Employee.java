package gr.mycompany.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity

/**
 * Class employee with three attributes
 * Using @Entity creates a table from this class
 * Id number is the primary key
 */
public class Employee {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;
    private String name;
    private String address;


}
