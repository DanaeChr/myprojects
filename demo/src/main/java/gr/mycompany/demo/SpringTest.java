package gr.mycompany.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.List;

@Slf4j
public class SpringTest {

    public static final String REST_SERVICE_URI = "http://localhost:8080";

    private static void listAllEmployees () {

        RestTemplate restTemplate = new RestTemplate( );
        List < LinkedHashMap < String, Object > > employeesMap =
                restTemplate.getForObject( REST_SERVICE_URI + "/employee/", List.class );
        if (employeesMap!=null) {
            for (LinkedHashMap<String, Object> map : employeesMap) {
                log.info("Employee : id=" + map.get("id") + ", Name=" + map.get("name"));
            }
        }
        else {
            log.error("No such employees exist!");
        }

    }

    public static void deleteEmployee ( int id ) {
        RestTemplate restTemplate = new RestTemplate( );
        restTemplate.delete( REST_SERVICE_URI + "/employee/" + id );
    }

    public static void main ( String[] args ) {
        listAllEmployees();
        deleteEmployee(3);
        listAllEmployees();
    }
}
