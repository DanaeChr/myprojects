package gr.mycompany.demo.controller;

import lombok.extern.slf4j.Slf4j;
import gr.mycompany.demo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import gr.mycompany.demo.service.EmployeeService;
import util.EmployeeError;

import java.util.List;

@Slf4j
@RestController
//@RequestMapping(value = "/api/")
public class Controller {

    @Autowired
    EmployeeService EmployeeService;

    /**
     * Gets the string myreturn from localhost:8080/test
     * @return a string for the test http response
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<String> test(){
        log.debug( "Entering {}", "testme" );
        String myreturn = "test";
        log.debug( "Leaving {}","testme" );
        return new ResponseEntity <String>( myreturn, HttpStatus.OK );
    }

    /**
     * Gets the employee list using localhost:8080/employee
     * @return list of employees
     */
    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ResponseEntity< List<Employee> > listAllUsers() {
        List < Employee > employees = EmployeeService.findAllEmployees( );
        if (employees.isEmpty( )) {
            return new ResponseEntity( HttpStatus.NO_CONTENT );
        }
        return new ResponseEntity < List < Employee > >( employees, HttpStatus.OK );
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) {
        log.info("Fetching & Deleting Employee with id {}", id);

        Employee employees = EmployeeService.findEmployeeById(id);
        if (employees == null) {
            log.error("Unable to delete. Employee with id {} not found.", id);
            return new ResponseEntity(
                    new EmployeeError("Unable to delete. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        EmployeeService.deleteEmployeeById(id);
        return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> findEmployee(@PathVariable("id") int id) {
        log.info("Find Employee with id {}", id);
        Employee employees = EmployeeService.findEmployeeById(id);
        if (employees == null) {
            log.error("Employee with id {} not found.", id);
            return new ResponseEntity(
                    new EmployeeError("Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Employee>(employees, HttpStatus.OK);
    }

}
