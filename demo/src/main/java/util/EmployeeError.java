package util;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class EmployeeError extends Exception{
    private String errorMessage;


}
