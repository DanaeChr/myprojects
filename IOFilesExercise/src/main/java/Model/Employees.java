package Model;

public class Employees{
    private int id;
    private String name;
    private String address;
    private static int counter;


    public Employees ( int id, String name, String address ) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    //Getters
    public int getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public String getAddress() {

        return address;
    }

    //Setters

    public void setId(int id) {

        this.id = id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    @Override
    public String toString() {
        return id + ", " +
               name + ", " +
                address ;
    }
}

