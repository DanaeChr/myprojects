package Model;

import java.util.List;

public class Company {
    private String companyName;
    private List <Employees> employeeList;

    public Company ( String companyName, List <Employees> employeeList ) {
        this.companyName = companyName;
        this.employeeList = employeeList;
    }

    @Override
    public String toString () {
        String employeesString="";
        for (Employees emp:employeeList){
            employeesString += emp+ "\n\r";
        }
        return "Company Name: " +
                companyName + "\n\r" + employeesString ;
    }

    public void setEmployeeList ( List < Employees > employeeList ) {
        this.employeeList = employeeList;
    }

    public List < Employees > getEmployeeList () {
        return employeeList;
    }
}
