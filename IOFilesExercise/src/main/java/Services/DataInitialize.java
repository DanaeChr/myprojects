package Services;

import Model.Employees;

import java.util.ArrayList;
import java.util.List;

public class DataInitialize {

    public List<Employees> initializeData(){//different methods for different

        List < Employees > employeeList = new ArrayList <>();
        //Company company = new Company("Accenture", employeeList);

        employeeList.add(new Employees(1,"Athanasia", "Kapoy 28"));
        employeeList.add(new Employees(2,"Giannis", "Chalandri 50"));
        employeeList.add(new Employees(3,"Marios", "Papagou 89"));
        employeeList.add(new Employees(4,"Danae", "Kifisia 2"));
        employeeList.add(new Employees(5,"John", "Ilioupoli 63"));
        employeeList.add(new Employees(6,"Aliki", "Cholargos 74"));

        return employeeList;
    }
}
