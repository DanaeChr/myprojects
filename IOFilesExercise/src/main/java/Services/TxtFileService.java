package Services;
import Model.Employees;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TxtFileService {

    public void writeToTextFile( List < Employees > employeesList){
        PrintWriter pw;
        try {
            pw = new PrintWriter("list.txt");
            for (Employees emp : employeesList){
            pw.println( emp );
            }
            pw.close();
        } catch (FileNotFoundException e) {

            System.out.println("Error occured");
        }
    }
    public static List<Employees> readFromFile(String filename){

        List < Employees > employeeList = new ArrayList <>();
        try
                (Scanner scanner = new Scanner(new File("list.txt")))
            {
            while(scanner.hasNext()){
                String line;
                line = scanner.nextLine();
                String []words = line.split( "," );
               int id = Integer.parseInt(words[0].trim());
                String name = words[1].trim();
                String address = words[2].trim();

                Employees currentEmployee = new Employees(id, name, address );
                employeeList.add( currentEmployee );
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace( );
        }

        return employeeList;
    }

}
