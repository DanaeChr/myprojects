
import Model.Company;
import Model.Employees;
import Services.DataInitialize;
import Services.TxtFileService;

import java.util.List;

public class Main {

    public static void main (String [] args){

        DataInitialize init = new DataInitialize();

//Write to list.txt
//        List<Employees> employeeList = init.initializeData();
//         TxtFileService newFile = new TxtFileService();
//        newFile.writeToTextFile( employeeList );

//read from list.txt file and output to console
        List<Employees> employeeList = TxtFileService.readFromFile("list.txt");

        Company company = new Company( "My Company", employeeList );
        company.setEmployeeList(employeeList);

        System.out.println(employeeList);



    }
}
