import java.util.Scanner;

public class HeartRatesTester {
    public static void main( String[] args )
    {
        Scanner input = new Scanner( System.in );

        System.out.print( "Enter your first name: ");
        String firstName = input.nextLine();

        System.out.print( "Enter your last name: ");
        String lastName = input.nextLine();

        System.out.print( "Enter your age: ");
        int age = input.nextInt();

        HeartRates person = new HeartRates( firstName, lastName, age );

        System.out.printf( "Calculations for %s %s age %d\n",
                person.getFirstName(), person.getLastName(), person.getAge() );

        System.out.printf( "Maximum Heart Rate: %d BPM (beats per minute)\n", person.getMaxHeartRate() );

        System.out.print( "Target Heart Rate Range is: ");
        person.TargetHeartRateRange();
        System.out.println();
    }
}
