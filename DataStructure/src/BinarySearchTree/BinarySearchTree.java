package BinarySearchTree;

import java.util.Scanner;

public class BinarySearchTree
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        /* Creating object of BST */
        BST bst = new BST();
        char ch;
        /*  Perform tree operations  */
        do
        {
            System.out.print("=======================================================\n");
            System.out.print("           Binary Search Tree Operations Menu\n");
            System.out.print("=======================================================\n");
            System.out.println("1. Add a node ");
            System.out.println("2. Delete a node");
            System.out.println("3. Show min node");
            System.out.println("4. Show max node");
            System.out.println("5. Find a node");
            System.out.println("6. Print all nodes");
            System.out.println("7. Exit");
            System.out.print("Enter your choice: ");

            int choice = scan.nextInt();
            switch (choice)
            {
                case 1 :
                    System.out.print("Enter integer element to insert: ");
                    bst.insert( scan.nextInt() );
                    break;
                case 2 :
                    System.out.print("Enter integer element to delete: ");
                    bst.delete( scan.nextInt() );
                    break;
                case 3 :
                    System.out.println("Min node: "+ bst.findMin());
                    break;
                case 4 :
                    System.out.println("Max node: "+ bst.findMax());
                    break;
                case 5 :
                    System.out.print("Enter integer element to search: ");
                    System.out.print("Search result : "+ bst.search( scan.nextInt() ));
                    break;
                case 6 :
                    System.out.print("Number of Nodes: "+ bst.countNodes());
                    System.out.println("Post order : ");
                    bst.postorder();
                    System.out.println("Pre order : ");
                    bst.preorder();
                    System.out.println("In order : ");
                    bst.inorder();
                    break;
                case 7 :
                    System.exit(0);
                    break;
                default :
                    System.out.println("Wrong Entry \n ");
                    break;
            }


            System.out.print("Do you want to continue? (Type Y or N): ");
            ch = scan.next().charAt(0);
        } while (ch == 'Y'|| ch == 'y');
    }
}