package Arrays;

/**
 * Find Duplicates
 * The CountDuplicates program implements an application that
 * simply adds given integers numbers to an array, counts the
 * duplicate number and print the output on the screen.
 *
 * @author Danae Chronopoulou
 * @version 1
 * @since 2019-04-01
 */
public class CountDuplicates {

    /**
     * This method is used to add integers in the array
     * and returns the times that an integer is found.
     * @param array to add integers
     * @param n array length
     * @param x integer
     * @return int This returns
     */
    static int count(int array[], int n, int x) {
        int t = 0;
        for (int i = 0; i < n; i++)
            if (x == array[i])
                t++;
        return t;
    }

    /**
     * This is the main method which makes use of count method.
     * @param args Unused.
     */
    public static void main(String args[]) {
        int array[] = {2, 5, 5, 5, 5, 6, 6, 8, 9, 9, 9,};
        int n = array.length;
        int x = 5;
        System.out.println("Element 5 occurs " + count(array, n, x) + " times");
    }
}
