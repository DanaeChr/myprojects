package Arrays;

/**
 * Find Pairs
 * The FindPairs program implements an application that
 * finds all the pairs in the given integer array where their sum
 * is equal to 10. It prints the output on the screen.
 *
 * @author Danae Chronopoulou
 * @version 1
 * @since 2019-04-01
 */
public class FindPairs {

    /**
     * This is the main method which makes use of the printPairs method.
     * @param args Unused
     */
    public static void main(String args[]) {


        int array[] = {8, 7, 2, 5, 3, 1};
        int number = array.length;
        int sum = 10;
        printPairs(array, number, sum);
    }

    /**
     * This method is used to find all possible pairs where their
     * sum is equal to 10. It gets the given array and creates
     * another array where it checks the sum and stores the pairs.
     * @param array To read and write the pairs
     * @param number The array length
     * @param sum The sum of two numbers
     */
    static void printPairs(int array[], int number, int sum) {
        for (int i = 0; i < number; i++)
            for (int j = i + 1; j < number; j++)
                if (array[i] + array[j] == sum)
                    System.out.println("Pair found at index " + i + " and " + j + "(" + array[i] + "+" + array[j] + ")");
    }
}
