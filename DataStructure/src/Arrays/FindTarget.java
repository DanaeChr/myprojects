package Arrays;

import java.util.Arrays;

/**
 * FindTarget
 * The FindTarget program implements an application that
 * finds the index of a given number inside an array and
 * prints the output on the screen.
 *
 * * @author Danae Chronopoulou
 *  * @version 1
 *  * @since 2019-04-01
 */
public class FindTarget {

    /**
     * This method search the index of a target using Linear search.
     * A linear search scans one item at a time, without jumping to any item.
     *
     * @param array Given array with integers
     * @param t target number
     * @return index or -1 if array is empty
     */
    public static int findIndexUsinglinear(int array[], int t) {
        if (array == null) {
            System.out.println("The array is empty");
        }

        int len = array.length;
        int i = 0;

        while (i < len) {

            if (array[i] == t) {
                return i;
            } else {
                i = i + 1;
            }
        }
        return -1;
    }

    /**
     * This method search the index of a target using Binary search.
     * A binary search cut down your search to half as soon as you
     * find middle of a sorted list
     *
     * @param array Search in the array
     * @param t the target
     * @return index or -1 if array is empty
     */
    public static int findIndexUsingBinary(int array[], int t) {

        int index = Arrays.binarySearch(array, t);
        return (index < 0) ? -1 : index;
    }

    /**
     * This is the main method which makes use of findIndexUsingBinary
     * and findIndexUsingLinear methods
     *
     * @param args Unused
     */
    public static void main(String[] args) {
        int[] array = {2, 3, 5, 7, 9};

        // find the index of 9
        System.out.println("(Linear Search) Index position of 9 is: " + findIndexUsinglinear(array, 9));

        // find the index of 9
        System.out.println("(Binary Search) Index position of 9 is: " + findIndexUsingBinary(array, 9));

    }
}