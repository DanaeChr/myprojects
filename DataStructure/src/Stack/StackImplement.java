package Stack;

import java.util.Scanner;

public class StackImplement
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        //System.out.println("Enter Size of Integer Stack ");
        //int n = scan.nextInt();
        /* Creating object of class arrayStack */
        MyStack stk = new MyStack(10);
        /* Perform Stack Operations */
        char ch;
        do{


            System.out.print("================================================\n");
            System.out.print("           Stack Operations Menu\n");
            System.out.print("================================================\n");
            System.out.println("1. Add items (Push) ");
            System.out.println("2. Delete items (Pop) ");
            System.out.println("3. Show the number of items ");
            System.out.println("4. Show min and max items");
            System.out.println("5. Find an item");
            System.out.println("6. Print all items");
            System.out.println("7. Exit");
            System.out.print("Enter your choice: ");


            int choice = scan.nextInt();
            switch (choice)
            {
                case 1 :
                    System.out.print("Enter integer to add: ");
                    try
                    {
                        stk.push( scan.nextInt() );
                    }
                    catch (Exception e)
                    {
                        System.out.println("Error : " + e.getMessage());
                    }
                    break;
                case 2 :
                    try
                    {
                        System.out.println("Deleted integer: " + stk.pop());
                    }
                    catch (Exception e)
                    {
                        System.out.println("Error : " + e.getMessage());
                    }
                    break;
                case 3 :
                    System.out.println("Number of items: " + stk.getSize());

                    break;
                case 4 :
                    System.out.println("Min Max TODO ");
                    break;
                case 5 :
                    try
                    {
                        System.out.println("Peek Integer: " + stk.peek());
                    }
                    catch (Exception e)
                    {
                        System.out.println("Error : " + e.getMessage());
                    }
                    break;
                case 6 :
                    stk.display();
                    break;
                default :
                    System.out.println("Wrong Entry \n ");
                    break;
            }


            System.out.print("Do you want to continue? (Type Y or N): ");
            ch = scan.next().charAt(0);

        } while (ch == 'Y'|| ch == 'y');
    }
}