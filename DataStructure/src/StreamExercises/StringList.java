package StreamExercises;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringList {
    public static void main(String[] args) {


        List<String> originalList = Arrays.asList("Stream", "Java", "", "Hello", "45879", "abcd", "", "Danae");
        System.out.println(originalList);

        List<String> filteredList = originalList.stream().filter(String->!String.isEmpty()).collect(Collectors.toList());
        System.out.println(filteredList);

    }
}
