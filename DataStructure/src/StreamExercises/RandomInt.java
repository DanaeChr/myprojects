package StreamExercises;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomInt {

        public static void main(String[] args) {
            Random random = new Random();
            List<Integer> randomNumbers = random.ints(1000).sorted().boxed().collect( Collectors.toList());
            List<Integer> oddNumbers = randomNumbers.stream().filter(i -> i % 2 != 0).collect(Collectors.toList());

            int min = randomNumbers.get(0);
            int max = randomNumbers.get(randomNumbers.size()-1);
            int count = (int) randomNumbers.stream().count();
            double avg = randomNumbers.stream().mapToInt(i -> i).average().getAsDouble();

            int oddCount = (int) oddNumbers.stream().count();
            int evenCount = (int) randomNumbers.stream().filter(i -> i % 2 == 0).mapToInt(i -> i).count();

            System.out.println("-----------------------------------------");
            System.out.println("    Generated 1000 Random Integers: ");
            System.out.println("-----------------------------------------");
            System.out.println("Average: " + avg);
            System.out.println("The sum: " + count);
            System.out.println("Minimum: " + min);
            System.out.println("Maximum: " + max);
            System.out.println("Odd Numbers: " + oddCount);
            System.out.println("Even Numbers: " + evenCount );

        }
    }

