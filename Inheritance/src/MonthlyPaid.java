public class MonthlyPaid extends Employee implements Developer{
    private double salaryPerMonth;
    public MonthlyPaid() {
        super();
    }
    public MonthlyPaid(String name, String address,
                       double salaryPerMonth) {
        super(name, address);
        this.salaryPerMonth=salaryPerMonth;
    }
    @Override
    public String toString() {
        return super.toString()+"MonthlyPaid [salaryPerMonth=" +
                salaryPerMonth + "]" ;
    }
    public double getSalaryPerMonth() {
        return salaryPerMonth;
    }
    public void setSalaryPerMonth(double salaryPerMonth) {
        this.salaryPerMonth = salaryPerMonth;
    }
    @Override
    public boolean isProjectReady() {
        // TODO Auto-generated method stub
        return false;
    }
}