public class HourlyPaid extends Employee implements Director{
    public int getHoursPerWeek() {
        return hoursPerWeek;
    }
    public void setHoursPerWeek(int hoursPerWeek) {
        this.hoursPerWeek = hoursPerWeek;
    }
    public double getSalaryPerHour() {
        return salaryPerHour;
    }
    public void setSalaryPerHour(double salaryPerHour) {
        this.salaryPerHour = salaryPerHour;
    }
    private int hoursPerWeek;
    private double salaryPerHour;
    public HourlyPaid() {
        super();
    }
    public HourlyPaid(String name, String address, int hoursPerWeek,
                      double salaryPerHour) {
        super(name, address);
        this.hoursPerWeek=hoursPerWeek;
        this.salaryPerHour=salaryPerHour;
    }
    @Override
    public String toString() {
        return super.toString()+"HourlyPaid [hoursPerWeek=" +
                hoursPerWeek + ", salaryPerHour=" + salaryPerHour ;
    }
    @Override
    public void giveOrder(Employee toEmployee, String Command) {
        // TODO Auto-generated method stub

    }
}