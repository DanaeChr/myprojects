public interface Director {
    void giveOrder(Employee toEmployee, String Command);
}