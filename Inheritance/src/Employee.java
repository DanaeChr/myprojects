import java.util.Comparator;
import java.util.List;

public class Employee implements Comparable<Employee> {
    private int id;
    private String name;
    private String address;
    private static int counter;
    private double salary;

    public Employee ( String name, String address ) {
    }

    public int getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public void setName ( String name ) {
        this.name = name;
    }

    public String getAddress () {
        return address;
    }

    public void setAddress ( String address ) {
        this.address = address;
    }

    public Employee ( String name, String address, double salary ) {
        super( );
        counter++;
        id = counter;
        this.name = name;
        this.address = address;
    }

    public Employee () {
        super( );
        counter++;
        id = counter;
    }

    @Override
    public String toString () {
        return "Employee [id=" + id + ", name=" + name + ", address=" + address + "]";
    }

    @Override
    public int compareTo ( Employee o ) {
        return (int) (this.salary = o.getSalary( ));
    }

    public double getSalary () {
        return salary;
    }

    //Method with generic implementation
    public static < T extends Comparable < T > > T minimumSalary ( T[] elements ) {
        T lower = elements[0];
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].compareTo( lower ) < 0) {
                lower = elements[i];
            }

        }
        return lower;
    }
    public static < T extends Comparable < T > > T minimumSalary ( List<T> elements) {
        T lower = elements.get( 0 );
        for (T element: elements) {//or: int i = 0; i < elements.size(); i++) {
            if (element.compareTo( lower ) < 0) {
                lower = element;
            }
        }
        return lower;
    }

    public static < T , K extends Comparator < T > > T minByLength ( List<T> elements, K comparator) {
        T small = elements.get( 0 );
        for (T element: elements) {//or: int i = 0; i < elements.size(); i++) {

            if (comparator.compare( element , small )<0) {
                small = element;
            }
        }
        return small;
    }


}