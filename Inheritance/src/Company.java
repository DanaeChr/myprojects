import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author diracleous
 *
 */
/**
 * @author diracleous
 *
 */
public class Company {
    private String name;
    private List<Employee> employees;

    public Company(String name) {
        super();
        this.name = name;
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }



    /**
     * Displays at the standard output
     * the number of HourlyPaid employees
     * the number of which are paid above 20 euro per hour
     * the number of MonthlyPaid employees
     * the number of which are paid above 1000 euro per month
     */
    public void simpleStatistics() {
        int numHourly = 0;
        int numHourlyAbove20=0;
        int numMonthly=0;
        int numMonthlyAbove1000=0;

        for(Employee employee:employees)
        {
            if (employee instanceof HourlyPaid) {
                numHourly++;
                if (((HourlyPaid)employee).getSalaryPerHour()>20) {
                    numHourlyAbove20++;
                }
            }
            if (employee instanceof MonthlyPaid) {
                numMonthly++;
                if (((MonthlyPaid)employee).getSalaryPerMonth()>1000) {
                    numMonthlyAbove1000++;
                }
            }
        }
        PrintWriter pw;
        try {
            pw = new PrintWriter("employeeStats.txt");
            pw.println("numHourly:"+numHourly
                    +"numHourlyAbove20:"+numHourlyAbove20
                    +"numMonthly:"+numMonthly
                    +"numMonthlyAbove1000:"+numMonthlyAbove1000);
            pw.println("End of file");
            pw.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.out.println("Error occured");
        }
        System.out.println("numHourly:"+numHourly
                +"numHourlyAbove20:"+numHourlyAbove20
                +"numMonthly:"+numMonthly
                +"numMonthlyAbove1000:"+numMonthlyAbove1000);

    }

    @Override
    public String toString() {
        String employeeString="";
        for(Employee employee:employees) {
            employeeString+= employee+"\n\r";
            if (employee instanceof MonthlyPaid) {
                MonthlyPaid p=(MonthlyPaid)employee;
                System.out.println(p.isProjectReady());
            }

        }

        return "Company [name=" + name + ", \n\remployees=\n\r" +
                employeeString + "]";
    }

}