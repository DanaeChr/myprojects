package services;

import pojos.Item;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.*;
import java.util.List;

public class StaxWriter {
    public static void saveConfig(List<Item> items, String configFile) throws Exception {

        // create an XMLOutputFactory
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();

        // create XMLEventWriter
        XMLEventWriter eventWriter = outputFactory
                .createXMLEventWriter(new FileOutputStream(configFile));

        // create an EventFactory
        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent end = eventFactory.createDTD("\n");

        // create and write Start Tag
        StartDocument startDocument = eventFactory.createStartDocument();
        eventWriter.add(startDocument);

        // create config open tag
        StartElement configStartElement = eventFactory.createStartElement("",
                "", "config");
        eventWriter.add(configStartElement);
        eventWriter.add(end);

        // Write the different nodes
        for (Item item : items) {
            createNode(eventWriter,"item", "item");
            createNode(eventWriter, "mode", item.getMode());
            createNode(eventWriter, "unit", item.getUnit());
            createNode(eventWriter, "current", item.getCurrent());
            createNode(eventWriter, "interactive", item.getInteractive());
            eventWriter.add(end);
        }
        eventWriter.add(eventFactory.createEndElement("", "", "config"));
        eventWriter.add(end);
        eventWriter.add(eventFactory.createEndDocument());
        eventWriter.close();

    }

    private static void createNode(XMLEventWriter eventWriter, String name, String value) throws XMLStreamException {

        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
        XMLEvent end = eventFactory.createDTD("\n");
        XMLEvent tab = eventFactory.createDTD("\t");

        // create Start node
        StartElement sElement = eventFactory.createStartElement("", "", name);
        eventWriter.add(tab);
        eventWriter.add(sElement);

        // create Content
        Characters characters = eventFactory.createCharacters(value);
        eventWriter.add(characters);

        // create End node
        EndElement eElement = eventFactory.createEndElement("", "", name);
        eventWriter.add(eElement);
        eventWriter.add(end);
    }

//  public static void saveToJSON(List<Item> items, String fileName) {
//    JSONObject obj = new JSONObject();
//    obj.put("name", "List Of Items");
//
//    JSONArray list = new JSONArray();
//
//    for(Item item : items) {
//      list.add(item.toString() + "\n");
//    }
//    obj.put("messages", list);
//
//    try (FileWriter file = new FileWriter(fileName)) {
//      file.write(obj.toJSONString());
//      file.flush();
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//
//    System.out.print(obj);
//  }
//
  public static void saveToJSONWithJackson(List<Item> items) throws IOException {

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    ObjectMapper mapper = new ObjectMapper();

    mapper.writeValue(out, items);

    final byte[] data = out.toByteArray();
    System.out.println(new String(data));
  }
}
