package pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class Item {
    private String date;
    private String mode;
    private String unit;
    private String current;
    private String interactive;

    @Override
    public String toString() {
        return "Item{" + "date='" + date + '\'' + ", mode='"
                + mode + '\'' + ", unit='" + unit + '\''
                + ", current='" + current + '\'' + ", interactive='"
                + interactive + '\'' + '}';
    }
}
