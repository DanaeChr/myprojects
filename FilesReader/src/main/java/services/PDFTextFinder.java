package services;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import java.io.File;
import java.io.IOException;


public class PDFTextFinder {

    public void findTextInPDFDocument(String path, String word) throws IOException {

        File myFile = new File(path);
        PDDocument pdf = PDDocument.load(myFile);
        PDFTextStripper stripper = new PDFTextStripper();
        stripper.getCurrentPage();
        String text = stripper.getText(pdf);

        //prints the file and if the word is found
        for (int pageNum = 0; pageNum < pdf.getNumberOfPages(); pageNum++) {
            String str = ("Word: " + "'" + word + "'" + " " + "found: "  + text.contains(word) +" at page:" +" " +pageNum++);
            System.out.println(str);
        }

    }
}
