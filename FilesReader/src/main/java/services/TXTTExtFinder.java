package services;

import java.io.*;


public class TXTTExtFinder {

    public void findTextInTxtDocument(String path, String word)  {

        int count = 0,countBuffer=0,countLine=0;
        String lineNumber = "";
        String line;

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            try {
                while((line = br.readLine()) != null){

                    countLine++;
                    //System.out.println(line);
                    String[] txtwords = line.split(" ");

                    for (String str : txtwords) {
                        if (str.equals(word)) {
                            count++;
                            countBuffer++;
                        }
                    }

                    if(countBuffer > 0)
                    {
                        countBuffer = 0;
                        //  lineNumber += countLine + ",";
                        lineNumber = lineNumber +" "  + countLine + ". ";
                    }

                }
                br.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        System.out.println("Times found: "+count);
        System.out.println("Word found at lines: "+lineNumber);

    }
}
