
import services.MSWordTextFinder;
import services.PDFTextFinder;
import services.TXTTExtFinder;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

//ask's the user for a file path and for a word to search
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter a file path: ");
        String n = reader.next();
        System.out.print("Enter a Word to search: ");
        String z = reader.next();

//if the file is a pdf
        if (n.endsWith("pdf")) {

            PDFTextFinder q = new PDFTextFinder();
            q.findTextInPDFDocument(n, z);
            // System.out.println(q.findTextInPDFDocument(n, z));
        }
//if the file is a txt
        else if (n.endsWith("txt")) {
            TXTTExtFinder v = new TXTTExtFinder();
            v.findTextInTxtDocument(n, z);
            // System.out.println(v.findTextInTxtDocument(n, z));
        }
//if the file is a doc
        else{
            MSWordTextFinder k = new MSWordTextFinder();
            k.findTextInWordDocument(n, z);
            // System.out.println(k.findTextInWordDocument(n, z));

        }
    }

}
/*
C:\Users\Danae\Desktop\ToUpload\FilesReader\LoremIpsum.doc
C:\Users\Danae\Desktop\ToUpload\FilesReader\LoremIpsum.pdf
C:\Users\Danae\Desktop\ToUpload\FilesReader\LoremIpsum.txt
 */

