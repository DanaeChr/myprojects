package test;

import FactoryPattern.*;

import java.util.*;
import java.util.stream.Collectors;

public class FactoryPatternDemo {

    public static void main ( String[] args ) {
        Creator creator = new Creator();

        List <AbstractProduct> products = new ArrayList < AbstractProduct >(  );

        AbstractProduct service = creator.productFactoryMethod( TypeOfProduct.SERVICE );
        products.add( service );

        AbstractProduct product = creator.productFactoryMethod( TypeOfProduct.PRODUCT );
        products.add( product );

        AbstractProduct service1 = creator.productFactoryMethod( TypeOfProduct.SERVICE );
        products.add( service1 );

        AbstractProduct sub = creator.productFactoryMethod( TypeOfProduct.SUBSCRIPTION );
        products.add( sub );

        AbstractProduct product1 = creator.productFactoryMethod( TypeOfProduct.PRODUCT );
        products.add( product1 );

        AbstractProduct sub1 = creator.productFactoryMethod( TypeOfProduct.SUBSCRIPTION );
        products.add( sub1 );


        //System.out.println(products );

//        Iterator<AbstractProduct> productIterator = products.iterator();
//        while(productIterator.hasNext()){
//              AbstractProduct currentProduct = productIterator.next();
//            if(currentProduct instanceof Product){
//            System.out.println(productIterator.next());
//              }
//        }

        //System.out.println(products.stream().filter( productZ->productZ instanceof Product ).collect( Collectors.toList()) );


        System.out.println("Products: " );
        for(AbstractProduct ap:products){
            if(ap instanceof Product)
            System.out.println(ap);
        }
        System.out.println("Services: " );
        for(AbstractProduct ap:products){
            if(ap instanceof Service)
                System.out.println(ap );
        }
        System.out.println("Subscription: " );
        for(AbstractProduct ap:products){
            if(ap instanceof Subscription)
                System.out.println(ap );
        }

         }

    }
