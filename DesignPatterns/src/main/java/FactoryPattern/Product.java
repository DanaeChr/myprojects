package FactoryPattern;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString


public class Product extends AbstractProduct{
    private String provider;
    private String size;
}
