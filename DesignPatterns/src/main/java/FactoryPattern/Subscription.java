package FactoryPattern;

import lombok.*;

import java.util.Date;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Subscription extends AbstractProduct{
   private Date fromDate;
   private Date toDate;

}
