package FactoryPattern;


import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString


public abstract class AbstractProduct {
   private int id;
   private String name;
   private double price;



}
