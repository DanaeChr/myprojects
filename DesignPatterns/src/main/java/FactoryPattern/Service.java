package FactoryPattern;

import lombok.*;

import java.util.Date;

@Getter@Setter
@AllArgsConstructor
@NoArgsConstructor@ToString

public class Service extends AbstractProduct{
    private Date dateOfDelivery;
}
