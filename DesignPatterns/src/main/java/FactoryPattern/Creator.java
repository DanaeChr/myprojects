package FactoryPattern;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString

public class Creator {

    public AbstractProduct productFactoryMethod(TypeOfProduct typeOfProduct)
    //throws ProductCreationException
    {
        switch ( typeOfProduct ){
            case PRODUCT: return new Product();

            case SERVICE: return new Service();

            case SUBSCRIPTION: return  new Subscription();

            //default:   throw new ProductCreationException(  ); //or return null;
            default:return null;
        }
    }
}
