package FactoryPattern;

import lombok.Getter;


@Getter

public enum TypeOfProduct {
    PRODUCT, SERVICE, SUBSCRIPTION;
}
